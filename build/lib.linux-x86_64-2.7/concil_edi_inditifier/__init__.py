import re
import linecache


class EdiIdentifier:

    @staticmethod
    def identify_by_file(file_path, rules, *args):
        if len(args) > 0:
            line_pos = args[0]
        else:
            line_pos = 1

        line = linecache.getline(file_path, line_pos)
        for rule in rules:
            if re.match(rule.get('re_rule'), line) is not None:
                return rule.get('type')

        return None

    @staticmethod
    def identify_by_line(line, rules):
        for rule in rules:
            if re.match(rule.get('re_rule'), line) is not None:
                return rule.get('type')

        return None
