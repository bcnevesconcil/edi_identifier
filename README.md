# EDI Identifier - identify EDI files

A generic identifier for EDI files to tranlate it to JSON files 

## Assumptions:

+ Assuming python is installed on your system.
+ Assuming pip is installed on your system.

## import

    import from concil_edi_identifier import EdiIdentifier

## Definitions

**identify_by_file**

Params(`file_path`, `rules`, `*args`)

`file_path` is the location of the file you're trying to identify, the `rules` are passed as a list of dicts even if its only one, the dictionary must have `type` attribute for identification which will be returned if the docfile matches, a `startswith` attribute to eliminate obvious wrong files, and at last a regular expression that mathces the entire line of the file in a attribute called `re_rule`.
in the args you can pass the line of the file where you'll apply the regex rule to identify its origin (default 1)

**identify_by_line**

Params(`line`, `rules`)

start with the `line` of the file you want to identify, and input the `rules` is a list of dicts even if its only one, the dictionary must have `type` attribute for identification which will be returned if the docfile matches, a `startswith` attribute to eliminate obvious wrong files, and at last a regular expression that mathces the entire line of the file in a attribute called `re_rule`. 